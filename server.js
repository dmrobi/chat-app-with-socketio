var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io').listen(server);

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});

server.listen(3000, function(){
	console.log('Listening on: ' + server.address().port);
});

io.on('connection', function(socket){
	console.log('New Connection: ' + socket.id);

	socket.on('chatmsg', function(msg){
		console.log('Message: '+ msg + ' from ' + socket.id);
		socket.user = {
			id:socket.id,
			msg:msg
		};
		io.emit('chatmsg', socket.user);
	});


	socket.on('disconnect', function(){
		console.log('Disconnected: ' + socket.id);
	});
});